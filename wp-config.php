<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p]{NHo{48`Bh`IK6.j Tml|_k*&&nK$CMx2T;lVa]A~-}qDupEXu/-/<z=X|~Vm!' );
define( 'SECURE_AUTH_KEY',  'c~KtsD#vc55}z;pl/W$+,$eYE0Ay(sb`SR*0zR?UeN41D0VS~+[U:6q+gOXeyr|u' );
define( 'LOGGED_IN_KEY',    ')~1x{t_ImcvqXA.ud;b?h>)z[vLn<9W?/NR^XBY%naUKaj](ug@7_}D~NTd:c&]`' );
define( 'NONCE_KEY',        'DrFv?@j#@YxvNg3dkQ>n:Op0:P~K -omeQ7QKcR0Kzav#P9QiwBo}^6kEFS%8s5;' );
define( 'AUTH_SALT',        '7tg{yGxF@b%rt8yB! )U~P*qZE)g2V|rFlEQH,]h2Qght~!U?m:Ysu3Bu.BgET0M' );
define( 'SECURE_AUTH_SALT', '{5Fiv9)a)T7V#h8yd 2#2<$8^a#j%R<k}:s8k5yam9(p?MxKJ7NFlrQ]H]!yUmTG' );
define( 'LOGGED_IN_SALT',   '&]EhXrRP{{ws2k>i&oR#*9-]sR<*:bE)9gL{Bp SOw=(d|*N|<J1;>`lq|n@?I-~' );
define( 'NONCE_SALT',       'T*W0]o&-J}FkK*:#L2p3K]p~$q0cR43oN0h=ezNVWZ=w)X5 9bfcskTgFj,fjqtH' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
